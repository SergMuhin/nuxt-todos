import Vue from 'vue';
import api from '~/api';
import { NOTIFICATION_ERROR_ICON } from '~/constants';

export default {
  state: () => ({
    todos: null,
  }),

  actions: {
    async getTodos({ commit }) {
      try {
        const { data } = await api.getTodos();
        commit('setTodos', data);
      } catch (e) {
        Vue.toasted.error('Error while fetching todos', {
          icon: NOTIFICATION_ERROR_ICON
        });
      }
    },

    async createTodo({ commit }, todoItem) {
      try {
        commit('app/showLoading', true, { root: true });

        const { data } = await api.createTodo(todoItem);

        commit('createTodo', data);

        Vue.toasted.success('Todo created successfully!');
      } catch (e) {
        Vue.toasted.error('Error during todo creation', {
          icon: NOTIFICATION_ERROR_ICON
        });
      } finally {
        commit('app/showLoading', false, { root: true });
      }
    },

    async updateTodo({ commit }, todoItem) {
      try {
        commit('app/showLoading', true, { root: true });

        const { data } = await api.updateTodo(todoItem);

        commit('updateTodo', data);

        Vue.toasted.success('Todo updated successfully!');
      } catch (e) {
        Vue.toasted.error('Error during todo updating', {
          icon: NOTIFICATION_ERROR_ICON
        });
      } finally {
        commit('app/showLoading', false, { root: true });
      }
    },

    async deleteTodo({ commit }, todoItem) {
      try {
        commit('app/showLoading', true, { root: true });

        const { data } = await api.deleteTodo(todoItem);

        commit('deleteTodo', data);

        Vue.toasted.success('Todo deleted successfully!');
      } catch (e) {
        Vue.toasted.error('Error during todo deleting', {
          icon: NOTIFICATION_ERROR_ICON
        });
      } finally {
        commit('app/showLoading', false, { root: true });
      }
    },

    async changeTodoTaskStatus({ commit }, { todoItemId, taskId, status }) {
      try {
        const { data } = await api.changeTodoTaskStatus({ todoItemId, taskId, status });
        commit('changeTodoTaskStatus', data);
      } catch (e) {
        Vue.toasted.error('Error during changing task status', {
          icon: NOTIFICATION_ERROR_ICON
        });
      }
    },
  },

  mutations: {
    setTodos(state, todos) {
      state.todos = todos;
    },

    createTodo(state, todoItem) {
      state.todos.push(todoItem);
    },

    updateTodo(state, todoItem) {
      const index = state.todos.findIndex(item => item.id === todoItem.id);
      Vue.set(state.todos, index, todoItem);
    },

    deleteTodo(state, todoItem) {
      const index = state.todos.findIndex(item => item.id === todoItem.id);
      state.todos.splice(index, 1);
    },

    changeTodoTaskStatus(state, { todoItemId, taskId, status }) {
      const index = state.todos.findIndex(item => item.id === todoItemId);
      const taskIndex = state.todos[index].tasks.findIndex(item => item.id === taskId);
      Vue.set(state.todos[index].tasks[taskIndex], 'done', status);
    }
  }
};
