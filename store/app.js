export default {
  state: () => ({
    globalLoading: false,
  }),

  mutations: {
    showLoading(state, status) {
      state.globalLoading = status;
    },
  }
};
