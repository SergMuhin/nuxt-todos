import todos from './data/todos';

export default {
  getTodos: async () => new Promise(res => {
    setTimeout(() => res({ data: todos }), 400);
  }),

  createTodo: async (todoItem) => new Promise(res => {
    setTimeout(() => res({ data: todoItem }), 400);
  }),

  updateTodo: async (todoItem) => new Promise(res => {
    setTimeout(() => res({ data: todoItem }), 400);
  }),

  deleteTodo: async (todoItem) => new Promise(res => {
    setTimeout(() => res({ data: todoItem }), 400);
  }),

  changeTodoTaskStatus: async (data) => new Promise(res => {
    setTimeout(() => res({ data }), 50);
  })
};
