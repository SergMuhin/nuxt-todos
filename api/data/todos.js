import { v4 as uuidv4 } from 'uuid';

export default [
  {
    id: uuidv4(),
    title: 'First Todo Card',
    subtitle: 'Description',
    tasks: [
      {
        id: uuidv4(),
        title: 'Add new todo',
        done: false
      },
    ]
  },
  {
    id: uuidv4(),
    title: 'Sample project',
    subtitle: 'Due date: this month',
    tasks: [
      {
        id: uuidv4(),
        title: 'Implement scroll loading',
        done: false
      },
      {
        id: uuidv4(),
        title: 'Change main color',
        done: true
      },
      {
        id: uuidv4(),
        title: 'Fix error handling',
        done: false
      },
      {
        id: uuidv4(),
        title: 'Remove modules',
        done: true
      }
    ]
  },
  {
    id: uuidv4(),
    title: 'Read books',
    tasks: [
      {
        id: uuidv4(),
        title: 'Scientific Software Engineering in a Nutshell',
        done: false
      },
      {
        id: uuidv4(),
        title: 'Kindle Fire Development Essentials',
        done: true
      },
      {
        id: uuidv4(),
        title: '97 Things Every Software Architect Should Know',
        done: false
      }
    ]
  },
  {
    id: uuidv4(),
    title: 'Go shopping',
    subtitle: 'Products and household appliances',
    tasks: [
      {
        id: uuidv4(),
        title: 'Potatoes',
        done: true
      },
      {
        id: uuidv4(),
        title: 'Onion',
        done: false
      },
      {
        id: uuidv4(),
        title: 'Beef',
        done: true
      },
      {
        id: uuidv4(),
        title: 'Kettle',
        done: false
      },
      {
        id: uuidv4(),
        title: 'Blender',
        done: false
      }
    ]
  },
  {
    id: uuidv4(),
    title: 'Home affairs',
    tasks: [
      {
        id: uuidv4(),
        title: 'Clean up the kitchen',
        done: true
      },
      {
        id: uuidv4(),
        title: 'Fix the fence',
        done: false
      }
    ]
  }
];
